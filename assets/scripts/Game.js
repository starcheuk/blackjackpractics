var players = require('PlayerData').players;

var Game = cc.Class({
    extends: cc.Component,

    properties: {
        playerAnchors: {
            default: [],
            type: cc.Node
        },
        playerPrefab: cc.Prefab,
        assetMng: cc.Node,
        turnDuration: 0,
    },

    statics: {
        instance: null
    },

    onLoad: function () {
        Game.instance = this;
        this.assetMng = this.assetMng.getComponent('AssetMng');

        // create players
        this.player = null;
        this.createPlayers();

        // init logic
        
    },

    createPlayers: function () {
        for (let i = 0; i < 5; i++) {
            var playerNode = cc.instantiate(this.playerPrefab);
            var anchor = this.playerAnchors[i];
            var switchSide = (i > 2);
            anchor.addChild(playerNode);
            playerNode.position = cc.p(0, 0);

            var playerInfoPos = cc.find('anchorPlayerInfo', anchor).getPosition();
            var stakePos = cc.find('anchorStake', anchor).getPosition();
            var actorRenderer = playerNode.getComponent('ActorRenderer');
            actorRenderer.init(players[i], playerInfoPos, stakePos, this.turnDuration, switchSide);
            if (i === 2) {
                this.player = playerNode.getComponent('Player');
                this.player.init();
            }
        }
    },
});
