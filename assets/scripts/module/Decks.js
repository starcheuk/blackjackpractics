var Types = require('Types');

/*
* 扑克管理類, 用來管理一副或多副牌
* @class Decks
* @constructor
* @param {number} numberOfDecks - 總共幾副牌
*/
function Decks (numberOfDecks) {
    // 總共有幾副牌
    this._numberOfDecks = numberOfDecks;
    // 還沒發出去的牌
    this._cardIds = new Array(numberOfDecks * 52);

    this.reset();
}

Decks.prototype.reset = function () {
    this._cardIds.length = this._numberOfDecks * 52;
    var index = 0;
    
}
