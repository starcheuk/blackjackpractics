var Suit = cc.Enum({
    Spade: 1,   // 葵扇
    Heart: 2,   // 紅心
    Club: 3,    // 梅花
    Diamond: 4, // 階磚
});

var A2_10JQK = 'NAN,A,2,3,4,5,6,7,8,9,10,J,Q,K'.split(',');

/*
* 扑克牌類, 只用來表示牌的基本屬性, 不包含遊戲邏輯, 所有屬性只讀,
* 因此全局只需要有 52 個實例 (去掉大小王), 不論有多少副牌
* @class Card
* @constuctor
* @param {Number} point - 可能的值為 1 到 13
* @param {Suit} suit
*/
function Card (point, suit) {
    Object.defineProperties(this, {
        point: {
            value: point,
            writable: false
        },
        suit: {
            value: suit,
            writable: false
        },
        /*
        * @property {Number} id - 可能的值為 0 到 51
        */
        id: {
            value: (suit - 1) * 13 + (point - 1),
            writable: false
        },
        pointName: {
            get function () {
                return Suit[this.suit];
            }
        },
        suitName: {
            get: function () {
                return Suit[this.suit];
            }
        },
        isBlackSuit: {
            get: function () {
                return this.suit === Suit.Spade || this.suit === Suit.Club;
            }
        },
        isRedSuit: {
            get: function () {
                return this.suit === Suit.Heart || this.suit === Suit.Diamond;
            }
        },
    });
}

// 手中牌的狀態
var ActorPlayingState = cc.Enum({
    Normal: -1,
    Stand: -1, // 停牌
    Report: -1, // 報到
    Bust: -1, // 爆牌
})

var Hand = cc.Enum({
    Normal: -1,    // 無
    BlackJack: -1, // BlackJack (21點)
    FiveCard: -1,  // 五龍
});

module.exports = {
    Suit: Suit,
    Hand: Hand,
    ActorPlayingState: ActorPlayingState, 
};