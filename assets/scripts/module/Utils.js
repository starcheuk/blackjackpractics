
// 返回盡可能不超過 21 點的最小和最大點數
function getMinMaxPoint (cards) {
    var hasAce = false;
    var min = 0;
    for (var i = 0; i < cards.length; i++) {
        var card = cards[i];
        if (card.point === 1) {
            hasAce = true;
        }
        min += Math.min(10, card.point);
    }
    var max = min;
    // 如果有 1 个 A 可以當成 11
    if (hasAce && min + 10 <= 21) {
        // （如果兩個 A 都當成 11，那么總分最小也會是 22，爆了，所以最多只能有一个 A 當成 11）
        max += 10;
    }

    return {
        min: min,
        max: max
    };
}

function isBust (cards) {
    var sum = 0;
    for (let i = 0; i < cards.length; i++) {
        const card = cards[i];
        sum += Math.min(10, card.point);
    }
    return sum > 21;
}

module.exports = {
    getMinMaxPoint: getMinMaxPoint,
    isBust: isBust,
};
